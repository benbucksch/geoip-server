FROM node
WORKDIR /app/
COPY package*.json ./
RUN npm install
RUN npm run-script updatedb
COPY . .
EXPOSE 4532
CMD [ "npm", "start" ]
