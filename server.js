const geoip = require("geoip-lite");
const countries = require("./country-names.json");
const express = require("express");
const app = express();
const port = 4532;
const requestingSite = "https://www.beonex.com";

// CORS to allow our website to call us
function fromWebsite(request, response, next) {
  response.header("Access-Control-Allow-Origin", requestingSite);
  response.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
  response.header("Access-Control-Allow-Headers", "Content-Type");
  next();
};
app.use(fromWebsite);
app.options("/*");

app.get("/me", async function(request, response) {
  try {
    var ip = request.get("x-forwarded-for") || request.ip;
    var geo = geoip.lookup(ip);
    enrichData(geo);
    response.json(geo);
  } catch (ex) {
    console.error(ex);
    response.sendStatus(500);
  }
});

app.get("/:ipaddress", async function(request, response) {
  try {
    var ip = request.params.ipaddress;
    var geo = geoip.lookup(ip);
    enrichData(geo);
    response.json(geo);
  } catch (ex) {
    console.error(ex);
    response.sendStatus(500);
  }
});

function enrichData(geo) {
  if (geo.country) {
    geo.countryEnglish = countries[geo.country];
  }
}

app.listen(port, () => console.log(`Listening on port ${port}`))
